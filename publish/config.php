<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'default' => [
        'app_id' => env('WECHAT_MINE_APPID', 'wx73f9f9ab41b525f9'),
        'app_secret' => env('WECHAT_MINE_SECRET', '8b9760beca421e8e4662c576285a8be5'),
        'qrcode_path' => env('WXAPP_QRCODE_PATH', BASE_PATH . '/public/qrcode/'),
    ],
    'other' => [],
];
